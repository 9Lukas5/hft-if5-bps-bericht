# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.1.0](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/compare/v1.0.0...v1.1.0) (2019-11-10)


### Features

* enable automatic release generation on tags ([55d32c1](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/55d32c1))

## [1.0.0](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/compare/v0.2.0...v1.0.0) (2019-08-08)


### Bug Fixes

* **company/general:** Das Schreiben groß geschrieben ([e949f10](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/e949f10)), closes [#67](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/67)
* **company/general:** Komma ergänzt ([437f264](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/437f264))
* **company/general:** Übernahme groß geschrieben ([bd92efc](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/bd92efc)), closes [#54](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/54)
* **company/general:** Vielzahl groß geschrieben, Komma gesetzt ([178d1cb](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/178d1cb)), closes [#76](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/76)
* **company/general:** Zeichensetzung korrigiert ([07c32b9](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/07c32b9))
* **content:** portfolio spelled wrong ([6199202](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/6199202)), closes [#63](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/63)
* **content:** strategic spelled wrong ([54e1667](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/54e1667)), closes [#62](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/62)
* **tasks/IaC/alternatives:** komma zuviel und Schreibfehler ([6222f73](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/6222f73)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/currentState:** fehlendes Bindewort ergänzt ([d9a3922](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/d9a3922)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64) [#70](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/70)
* **tasks/IaC/implementation:** 'Nach (dem) Erreichen...' groß geschrieben ([6b73d03](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/6b73d03)), closes [#54](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/54)
* **tasks/IaC/implementation:** applikationsweite klein geschrieben ([f40ebaf](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/f40ebaf)), closes [#54](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/54)
* **tasks/IaC/implementation:** Beispielanwendungen zusammen geschrieben ([3ac32f0](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/3ac32f0)), closes [#75](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/75)
* **tasks/IaC/implementation:** Das Ändern groß geschrieben ([5afab1c](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/5afab1c)), closes [#74](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/74)
* **tasks/IaC/implementation:** Des Weiteren groß geschrieben ([69adbf3](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/69adbf3)), closes [#54](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/54)
* **tasks/IaC/implementation:** ein => eine ([f041770](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/f041770)), closes [#73](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/73)
* **tasks/IaC/implementation:** falschen korrigiert ([9159af8](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/9159af8)), closes [#54](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/54)
* **tasks/IaC/implementation:** fälschlicherweise Mehrzahl genutzt ([ccf75a4](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/ccf75a4)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/implementation:** kritischen klein geschrieben ([a96164d](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/a96164d)), closes [#54](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/54)
* **tasks/IaC/implementation:** pflegen klein geschrieben ([f067a9e](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/f067a9e)), closes [#54](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/54)
* **tasks/IaC/motivation:** Verlinkung -> Verlinkungen ([7eb1a51](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/7eb1a51))
* **tasks/IaC/problem:** erstellen => erstellten Typo fixed ([c887169](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/c887169)), closes [#69](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/69)
* **tasks/IaC/startingSituation:** Erzeugen ist Subjektiv ([cbf962d](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/cbf962d))
* **tasks/IaC/startingSituation:** Komma ergänzt ([e37e1f0](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/e37e1f0))
* **tasks/IaC/startingSituation:** Komma ergänzt ([3d4b39b](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/3d4b39b))
* **tasks/IaC/startingSituation:** Komma ergänzt ([3a29dc8](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/3a29dc8))
* **tasks/IaC/startingSituation:** Komma zuviel ([983c035](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/983c035))
* **tasks/IaC/startingSituation:** Komma zuviel ([915407c](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/915407c))
* **tasks/onboarding:** eigentlich -> eigentlichen ([638aa13](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/638aa13))
* **tasks/onboarding:** Komma ergänzt ([b8c809d](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/b8c809d))


### Content Changes

* **assets:** add stack categorizing example table ([00c6ed9](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/00c6ed9))
* **company/general:** rephrase analytics sentence ([d8f1f04](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/d8f1f04)), closes [#56](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/56)
* **company/general:** review and small phrasing changes ([395935e](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/395935e))
* **company/section:** add explanation for tomcat usage ([adb813f](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/adb813f)), closes [#57](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/57)
* **introduction:** added preamble and introduction ([e68cb8d](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/e68cb8d)), closes [#77](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/77)
* **tasks/IaC/implementation:** added example table ([70b26eb](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/70b26eb))
* **terms:** added 'NPM' to terms table ([a7c59f2](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/a7c59f2))
* **titlepage:** Erwähne Firma auf Titleblatt ([69992e1](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/69992e1))
* **udwp:** too much personal opinion \o/ ([6e26dd9](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/6e26dd9))


### Features

* **bib:** add entry for Siemens history ([3c14d33](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/3c14d33))
* **bib:** add entry for Siemens RSC Dortmund-Eving ([098b1cd](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/098b1cd))
* **changelog:** add content type to versionrc ([c9fdd9f](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/c9fdd9f))
* **content:** add a bit historical info about Siemens at first ([ef3bf0a](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/ef3bf0a)), closes [#61](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/61)
* **content:** added 'Rollmaterial' to terms table ([a3f7dc4](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/a3f7dc4)), closes [#55](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/55)
* **content:** set title and author of document ([3f05570](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/3f05570))
* **content:** use 'Unternehmen' on company general section heading ([6895122](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/6895122)), closes [#60](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/60)
* **license:** add CC-BY-ND 4.0 License ([a1bbbe7](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/a1bbbe7))
* **template:** add command to set footer content ([4f9a418](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/4f9a418))
* **titlepage:** setze Abgabetermin auf spätestet möglichen Zeitpunkt ([ad25a57](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/ad25a57))
* add introduction ([6340c17](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/6340c17))
* add title and author to pdf if set ([d5a22ba](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/d5a22ba))


### Redactional Changes

* **assets/RailigentDevelopmentProcess:** Bericht ist in deutsch, Grafiken bitte auch ([7cdbc00](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/7cdbc00)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **assets/siemensStructure:** Bericht ist in deutsch, Grafiken bitte auch ([2758b5a](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/2758b5a)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **company/general:** Anmerkungen Prof integriert ([a9268f9](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/a9268f9)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **company/general:** füllwort reduzierung ([c63b1ab](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/c63b1ab)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **company/general:** umformulierung RRX Werkstatt ([4c9fe17](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/4c9fe17)), closes [#65](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/65)
* **company/general:** write MO CS instead of only CS ([19d5cc2](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/19d5cc2))
* **company/general:** write MO RS instead of only RS ([f2feb32](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/f2feb32))
* **company/section:** Anmerkungen Prof integriert ([1556eea](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/1556eea)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **company/section:** beschreibung Plattformapplikationen umgestellt ([75976c3](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/75976c3)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **company/section:** erste AWS verwendung um Abkürzung in Klammern ergänzt ([5999da9](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/5999da9)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **company/section:** repo server ausgeschrieben ([f6adf7c](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/f6adf7c)), closes [#66](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/66)
* **company/section:** use german word to describe ng content in springboot container ([e5209a2](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/e5209a2)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/alternatives:** Anmerkungen Prof integriert ([928709c](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/928709c)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/alternatives:** Füllwortreduzierung ([ea351db](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/ea351db)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/currentState:** Anmerkungen Prof integriert ([4602376](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/4602376)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/currentState:** doppelte Parameter konfiguration umformuliert ([7eaa328](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/7eaa328)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/currentState:** fazit über fehlkonfiguartionen umformuliert ([c5f35a8](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/c5f35a8)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/currentState:** Füllwortreduzierung ([1896e11](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/1896e11)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/currentState:** qualität aus vorlagen ersteller CFN umformuliert ([7057e3c](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/7057e3c)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/currentState:** replace hyphen with LaTeX Code '"=' ([3728b04](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/3728b04)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/currentState:** tls fehlkonfiguration umformuliert ([3d9f95a](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/3d9f95a)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/currentState:** Vorlagen Absatz umformuliert ([106f6d5](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/106f6d5)), closes [#70](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/70)
* **tasks/IaC/implementation:** added ref to example table figure ([285c9ab](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/285c9ab))
* **tasks/IaC/implementation:** Anmerkungen Prof integriert (1/n) ([b532f80](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/b532f80)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/implementation:** Anmerkungen Prof integriert (2/2) ([e09b22d](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/e09b22d)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/implementation:** DevOps Team einheitlich genutzt ([4c2f966](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/4c2f966)), closes [#58](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/58)
* **tasks/IaC/implementation:** eliminiere Blödsinn mit Umformulierung ([343adf1](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/343adf1)), closes [#54](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/54)
* **tasks/IaC/implementation:** eliminiere idiotensicher mit Umformulierung ([750d670](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/750d670)), closes [#54](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/54)
* **tasks/IaC/implementation:** eliminiere monströs mit Umformulierung ([462c5b9](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/462c5b9)), closes [#54](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/54)
* **tasks/IaC/implementation:** erste Verwendung von VSCode ausgeschrieben ([a5c9e9a](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/a5c9e9a)), closes [#72](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/72)
* **tasks/IaC/implementation:** review & Überarbeitung first Prod usage ([203fcaa](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/203fcaa)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/implementation:** review & Überarbeitung Refactoring (1/n) ([cddba95](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/cddba95)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/implementation:** review & Überarbeitung Refactoring (2/2) ([fde6a3a](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/fde6a3a)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/implementation:** review & Überarbeitung V1.0 ([6571a87](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/6571a87)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/implementation:** schiebe Ende als Zwischensatz nach vorn & ergänze Formulierung ([97ee936](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/97ee936))
* **tasks/IaC/implementation:** umformulierung 'quer gestellt' ([63deb2d](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/63deb2d)), closes [#71](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/71)
* **tasks/IaC/implementation:** umformulierung 'zu Bruch gehen' ([57f1cb2](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/57f1cb2)), closes [#54](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/54)
* **tasks/IaC/implementation:** umformulierung entstehung zusätzlicher Objekte ([de09a75](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/de09a75)), closes [#54](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/54)
* **tasks/IaC/implementation:** umformulierung UnitTest und Logging implementierung ([4f39247](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/4f39247)), closes [#54](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/54)
* **tasks/IaC/implementation:** umforumulierung Leerer Sätze ([9e9c898](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/9e9c898)), closes [#54](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/54)
* **tasks/IaC/motivation:** Anmerkungen Prof integriert ([3cd7337](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/3cd7337)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/motivation:** Füllwortreduzierung ([40612eb](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/40612eb)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/motivation:** Mehrfach-Konfiguration als Wortverbindung ([8c67d51](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/8c67d51))
* **tasks/IaC/onboarding:** Anmerkungen Prof integriert ([bb5f596](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/bb5f596)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/perspective:** Anmerkungen Prof integriert ([024222f](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/024222f)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/perspective:** review & Überarbeitung ([836d04f](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/836d04f)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/perspective:** Semantic-Versionierung Patch und Minor vertauscht ([5e108f7](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/5e108f7))
* **tasks/IaC/perspective:** Umformulierung Jenkins-Libs Migration ([45c57fe](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/45c57fe))
* **tasks/IaC/perspective:** Umformulierung Semantic-Version Einsatz ([866dc98](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/866dc98)), closes [#54](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/54)
* **tasks/IaC/problem:** Anmerkungen Prof integriert ([307c506](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/307c506)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/problem:** Füllwortreduzierung ([62147d7](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/62147d7)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/problem:** replace hyphen with LaTeX Code '"=' ([746134e](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/746134e)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/problem:** Synonyme für mehr Abwechslung ([0ea15d8](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/0ea15d8)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/problem:** Wartungsmöglichkeite von CFN komisch formuliert ([bfc5ad3](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/bfc5ad3)), closes [#54](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/54)
* **tasks/IaC/problem:** zu ausgedehnte Formulierung gekürzt ([07fe613](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/07fe613)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/result:** Anmerkungen Prof integriert ([0024ee6](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/0024ee6)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/result:** review & Überarbeitung Ergebnis ([f1a0471](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/f1a0471)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/startingSituation:** Anmerkungen Prof integriert ([6653e57](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/6653e57)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/startingSituation:** Füllwortreduzierung ([0d03f14](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/0d03f14)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/startingSituation:** rephrase a bit ([c70b558](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/c70b558)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/startingSituation:** replace hyphen with LaTeX Code '"=' ([5e200ef](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/5e200ef)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/startingSituation:** replace hyphen with LaTeX Code '"=' ([5ec9327](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/5ec9327)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/startingSituation:** replace hyphen with LaTeX Code '"=' ([4774434](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/4774434)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/startingSituation:** Vorteile von CFN umformuliert ([4f42abf](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/4f42abf)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* Ersetze alle Vorkommen von 'deploy' mit deutschen Begriffen ([b313988](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/b313988)), closes [#54](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/54)
* **tasks/IaC/toolset:** Anmerkungen Prof integriert ([598cb0d](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/598cb0d)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/IaC/toolset:** npm -> NPM ([303b1ca](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/303b1ca))
* **tasks/IaC/toolset:** review & Überarbeitung Toolset ([8f21f28](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/8f21f28)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/onboarding:** rephrase remaining paragraphs using less 'I' ([26c7b6e](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/26c7b6e)), closes [#59](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/59)
* **tasks/onboarding:** rephrase WBT training ([73f03cd](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/73f03cd)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/onboarding:** vergeleich bestehender repos umformuliert ([086c197](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/086c197)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/personalXP:** Anmerkungen Prof integriert ([12df60a](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/12df60a)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* **tasks/uDWP:** Anmerkungen Prof integriert ([53186b4](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/53186b4)), closes [#64](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/64)
* reorder first pages according to prof ([38db36b](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/38db36b))
* replace copyright note with license string ([78ee64f](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/78ee64f))
* set versionText with new command from style.tex ([b17f35a](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/b17f35a))
* split page numbering ([6a7f8b1](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/6a7f8b1))
* **terms:** update AWS description, update sort ([1b75823](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/1b75823))
* **titlepage:** Exakter Name der Fakultät ([c7cd08f](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/c7cd08f))


### Style Changes

* change colors to be a bit less vibrant ([76bd245](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/76bd245)), closes [#53](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/53)
* **header/footer:** define rule width ([f23bb24](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/f23bb24))
* make sure paragraph breaks have consistent size ([135fb98](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/135fb98)), closes [#52](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/52)
* **assets:** make Siemens AG a card and the sections children of it ([d5861a3](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/d5861a3))
* **bib:** don't sort bibliography entries ([9119058](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/9119058))
* **captions:** make captions sans ([187994c](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/187994c))
* **company/general:** remove ending page break ([c512085](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/c512085))
* **company/section:** make it again a bit smaller as of [#56](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/issues/56) ([9ddb275](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/9ddb275))
* **company/section:** use full width for image ([719b339](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/719b339))
* **diagrams:** made RG dev structure thinner to wrap text around ([c533fa6](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/c533fa6))
* place terms and introduction before TOC ([0a57ef3](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/0a57ef3))
* **header/footer:** define front & mainmatter styles ([9c9ba0d](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/9c9ba0d))
* **header/footer:** shorten title of document ([df3512d](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/df3512d))
* **meta:** make header and footer using sans family ([4f3ad45](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/4f3ad45))
* **tasks/IaC/implementation:** add additional empty line to end paragraph ([665a520](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/665a520))
* **tasks/IaC/implementation:** tweak commitstrip comic appearance ([e1f9e25](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/e1f9e25))
* **terms:** Ergänze Überschrift & kein Teil des TOC ([c027f11](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/c027f11))
* **titlepage:** Fakultät ~für~ ... ([1b1778f](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/1b1778f))
* **titlepage:** use sans font on titlepage aswell ([1ea672f](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/1ea672f))
* roman page numbering with capital letters ([8240ff3](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/8240ff3))



## [0.2.0](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/compare/v0.1.0...v0.2.0) (2019-07-25)


### Bug Fixes

* **style:** set font to 11pt (default was 10pt) ([1b4dbb4](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/1b4dbb4))


### Features

* **content:** implement title page ([35ba5f5](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/35ba5f5))


### Style Changes

* make header and footer everything small ([0ab6a6f](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/0ab6a6f))
* set caption size to footnotesize ([870233d](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/870233d))
* **titlepage:** make upper block left align aswell ([3eb724a](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/commit/3eb724a))



## 0.1.0 (2019-07-24)

First Release after writing to be reviewed
