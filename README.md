# Internship report from my computer science studies
## topic: object oriented Cloudformation deployment lib

[![pipeline status](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/badges/master/pipeline.svg)](https://gitlab.com/9Lukas5/hft-if5-bps-bericht/pipelines)
[![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-yellow.svg)](https://conventionalcommits.org)
[![Made with LaTeX](https://img.shields.io/badge/Made%20with-LaTeX-1f425f.svg)](https://www.latex-project.org)
[![license CC-BY-ND 4.0](https://img.shields.io/badge/license-CC--BY--ND%204.0-informational)](https://creativecommons.org/licenses/by-nd/4.0/)

This report is part of my bachelor studies in computer science at the college of technology Stuttgart.

For this whole setup different tools are setup together:

* LaTeX for the text itself
* PlantUML for the schematic graphics
* docker images holding the LaTeX setup
* GitLab-CI pipeline for:
  * retrieving an unique document version number based on Git
  * generating the images from the PlantUML code
  * generating the PDF from the sources
